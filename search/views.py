from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json

# Create your views here.
def page(request):
    return render(request, 'search/search.html')

def search(request):
    query = request.GET['search_key']
    search_url = 'https://www.googleapis.com/books/v1/volumes?q=' + query +'&maxResults=36'
    response = requests.get(search_url)

    json_result = json.loads(response.content)
    return JsonResponse(json_result, safe = False)