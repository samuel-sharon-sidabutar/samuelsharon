from django.urls import path

from . import views

app_name = 'search'

urlpatterns = [
    path('', views.page, name='page'),
    path('search', views.search, name='search'),
]