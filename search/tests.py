from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client

# Create your tests here.
class Test(TestCase):
    def test_url_exist(self):
        response = Client().get('/search/')
        self.assertEquals(response.status_code, 200)

    def test_text_complete(self):
        response = Client().get('/search/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Search for Your Favorite books", html_kembalian)

    def test_template_exist(self):
        response = Client().get('/search/')
        self.assertTemplateUsed(response, 'search/search.html')

    def test_url_jsongetter_exist(self):
        response = Client().get('/search/search?search_key=Harry Potter')
        self.assertEquals(response.status_code, 200)    

    def test_jsonresponse_is_correct(self):
        response = Client().get('/search/search?search_key=')
        response_content = response.content
        self.assertJSONEqual(
            response_content,
            {
            "error": {
                "code": 400,
                "message": "Missing query.",
                "errors": [
                {
                    "message": "Missing query.",
                    "domain": "global",
                    "reason": "queryRequired",
                    "location": "q",
                    "locationType": "parameter"
                }
                ]
            }
            }
        )