from django.test import TestCase, Client

# Create your tests here.
class Test(TestCase):
    #Register
    def test_url_register_exist(self):
        response = Client().get('/account/register/')
        self.assertEquals(response.status_code, 200)

    def test_complete_register(self):
        response = Client().get('/account/register/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Register", html_kembalian)
        self.assertIn("Username", html_kembalian)
        self.assertIn("Password", html_kembalian)

    def test_template_register_exist(self):
        response = Client().get('/account/register/')
        self.assertTemplateUsed(response, 'registration/register.html')

    #Halaman sambutan
    def test_url_welcomePage_exist(self):
        response = Client().get('/account/welcome/')
        self.assertEquals(response.status_code, 200)

    def test_complete_welcomePage(self):
        response = Client().get('/account/welcome/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Welcome", html_kembalian)

    def test_template_welcomePage_exist(self):
        response = Client().get('/account/welcome/')
        self.assertTemplateUsed(response, 'registration/welcome.html')

    #Form Class
    def test_url_change_add_class(self):
        response = Client().get('/classes/add/')
        self.assertEquals(response.status_code, 302)

    def test_url_change_delete_class(self):
        response = Client().get('/classes/delete/1')
        self.assertEquals(response.status_code, 302)
        