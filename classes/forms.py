from django import forms

from .models import Classes

class ClassesForm(forms.ModelForm):
    class Meta:
        model = Classes
        fields = [
            'matkul',
            'dosen',
            'sks',
            'desc',
            'semester',
            'ruang',
        ]
        widgets = {
            'matkul': forms.TextInput(attrs={'class': 'form-control'}),
            'dosen': forms.TextInput(attrs={'class': 'form-control'}),
            'sks': forms.TextInput(attrs={'class': 'form-control'}),
            'desc': forms.Textarea(attrs={'class': 'form-control'}),
            'semester': forms.TextInput(attrs={'class': 'form-control'}),
            'ruang': forms.TextInput(attrs={'class': 'form-control'}),
        }