from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('album', views.album, name='album'),
    path('oldIndex', views.oldIndex, name='oldIndex'),
]