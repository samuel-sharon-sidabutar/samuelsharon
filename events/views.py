from django.shortcuts import render
from django.views.generic import ListView, CreateView
from .models import Events, Members


# Create your views here.
class EventsView(ListView):
    context_object_name = 'list'
    template_name = 'events/events.html'
    queryset = Events.objects.all()

    def get_context_data(self, **kwargs):
        context = super(EventsView, self).get_context_data(**kwargs)
        context['members'] = Members.objects.all()
        return context


class AddEventView(CreateView):
    model = Events
    template_name = 'events/add-event.html'
    fields = '__all__'

class AddMemberView(CreateView):
    model = Members
    template_name = 'events/add-member.html'
    fields = '__all__'

# def test(request):
#     return render(request, 'events/test.html')