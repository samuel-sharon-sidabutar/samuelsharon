from django.db import models
from django.urls import reverse
# Create your models here.
class Events (models.Model):
    event = models.CharField(max_length=45)

    def __str__(self):
        return self.event

    def get_absolute_url(self):
        return reverse('events:list')


class Members (models.Model):
    member = models.CharField(max_length=45)
    event = models.ForeignKey(Events, on_delete=models.CASCADE)

    def __str__(self):
        return self.member

    def get_absolute_url(self):
        return reverse('events:list')
