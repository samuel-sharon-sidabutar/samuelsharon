from django.urls import path

from . import views
from .views import EventsView, AddEventView, AddMemberView

app_name = 'events'

urlpatterns = [
    path('', EventsView.as_view(), name='list'),
    path('add-member/', AddMemberView.as_view(), name='add-member'),
    path('add-event/', AddEventView.as_view(), name='add-event'),
    # path('add-event/POST', views.test, name = 'test'),
]